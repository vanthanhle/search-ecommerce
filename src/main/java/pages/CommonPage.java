package pages;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import common.Assertion;
import common.DateTime;
import common.Logger;
import common.Utils;
import common.Const.Configurations;
import common.model.Product;

public class CommonPage extends PageObjectBase{

	WebDriver driver;

	public CommonPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Go to specific page.
	 * 
	 * @param url: url.
	 */
	public void goToPage(String url) {
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
	}

	// Get the User name from Home Page
	public String getPageTitle() {
		return driver.getTitle();
	}

	//Sort product
	private List<Product> sortProduct(List<Product> original){
		List<Product> sorted = original;
		Collections.sort(sorted, new Comparator<Product>() {
			@Override
			public int compare(Product o1, Product o2) {
				return Double.compare(o1.getPrice(), o2.getPrice());
			}
		});
		return sorted;
	}
	
	/**
	 * Displayed and export all sorted data.
	 * @param original
	 */
	public void displayedTheResult(List<Product> original) {
		String path = String.format(Configurations.EXPORT_PATH, DateTime.getTimeNow("yyyyMMdd_HHmmss"));
		StringBuilder exportData = new StringBuilder();
		if (original != null) {
			List<Product> sorted = sortProduct(original);
			exportData.append(Logger.line()).append("\n");
			exportData.append(String.format(" %-7s| %-30.30s| %-10.10s| %s", "WEBSITE", "NAME", "PRICE", "LINK")).append("\n");
			exportData.append(Logger.line()).append("\n");
			for (Product product : sorted) {
				exportData.append(product.toString()).append("\n");
			}
			Utils.ExportDataToTextFile(path, exportData.toString());
		} else {
			Utils.ExportDataToTextFile(path, "No data found!");
		}
		System.out.println("\n-----------RESULT---------\n" + exportData);
	}

	/**
	 * -------------- VERIFY FUNCTIONS ---------------------------
	 */

	public void verifyTitle(String expected) {
		Assertion.AssertTrue(getPageTitle().contains(expected), "Title is not match");
	}

}
