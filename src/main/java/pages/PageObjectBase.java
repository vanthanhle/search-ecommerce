package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Utils;

public class PageObjectBase {
	
	/**
	 * Wait element displayed.
	 * @param driver
	 * @param element
	 * @param second
	 * @return
	 */
	public boolean waitElementDisplayed(WebDriver driver, WebElement element, int second) {
	  try {
	    WebDriverWait wait = new WebDriverWait(driver, second);
        wait.until(ExpectedConditions.visibilityOf(element));
        Utils.sleep(2);
        return true;
      } catch (Exception e) {
        return false;
      }
		
	}
	
	/**
	 * Check element is displayed.
	 * @param element
	 * @return
	 */
	public boolean isDisplayed(WebElement element) {
	  try {
	    return element.isDisplayed();
      } catch (Exception e) {
        return false;
      }
		
	}
	
	/**
	 * Check element is disable base on disable attribute.
	 * @param element
	 * @return
	 */
	public boolean isDisabled(WebElement element) {
	  try {
	    boolean isDisable = element.getAttribute("aria-disabled").equals("true");
	    return isDisable;
	  } catch (Exception e) {
	    return false;
	  }
	  
	}
	
	/**
	 * clear old data, input new data.
	 * @param element
	 * @param value
	 */
	public void enterToElement(WebElement element, String value) {
		element.clear();
		element.sendKeys(value);
	}
	
	/**
	 * Get text value.
	 * @param element
	 * @return text value
	 */
	public String getTextValue(WebElement element) {
	  
	  String value = element.getText();
      if (Utils.isNullOrEmpty(value)) {
        value = element.getAttribute("value");
      }
      if (Utils.isNullOrEmpty(value)) {
        value = element.getAttribute("textContent");
      }
      return value.trim();
    }

}
