package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Assertion;
import common.Report;
import common.Utils;
import common.model.Product;
import testcore.AppConfig;

public class EbayPage extends PageObjectBase{

	final private String EBAY = "Ebay";

	/**
	 * -------------- ELEMENTS ---------------------------
	 */
	WebDriver driver;
	CommonPage common;

	@FindBy(id = "nav-search-submit-text")
	WebElement btnSearch;

	@FindBy(xpath = "//input[@id = 'gh-ac']")
	WebElement txtSearchEbay;

	@FindBy(xpath = "(//div[@id='srp-river-results']//li[contains(@class,'s-item')])[1]")
	WebElement firstItem;

	@FindBy(xpath = "//h3[.='No exact matches found']")
	WebElement lblNoResult;

	@FindBy(xpath = "//a[@aria-label='Next page']")
	WebElement btnNextPage;

	private String parentItem = "//div[@id='srp-river-results']//li[contains(@class,'s-item')]";
	private String priceXpath = "(//div[@id='srp-river-results']//li[contains(@class,'s-item')])[%s]//span[@class= 's-item__price']";
	private String linkXpath = "(//div[@id='srp-river-results']//li[contains(@class,'s-item')])[%s]//a[@class='s-item__link']";
	private String nameXpath = "(//div[@id='srp-river-results']//li[contains(@class,'s-item')])[%s]//a[@class='s-item__link']//h3";
	private int numberOfSearchResult;

	public EbayPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		common = new CommonPage(driver);
	}

	/**
	 * -------------- ACTIONS ---------------------------
	 */

	/**
	 * @param value: use to enter to user name field .
	 */
	public void enterSearchValue(String value) {
		enterToElement(txtSearchEbay, value);
		Utils.sleep(AppConfig.SMALL_TIME);
	}

	/**
	 * click on search button or enter after input search field.
	 */
	public void clickOnSearchButton() {
		txtSearchEbay.sendKeys(Keys.ENTER);
		Utils.sleep(AppConfig.SMALL_TIME);
	}

	/**
	 * get result list.
	 * 
	 * @return result list.
	 */
	public List<Product> getSearchProduct() {
		if (numberOfSearchResult > 0) {
			List<Product> products = new ArrayList<Product>();

			boolean btnNextPageEnable = false;
			do {
				numberOfSearchResult = getNumberOfSearchItem();
				for (int i = 1; i <= numberOfSearchResult; i++) {
					products.add(new Product(EBAY, getLink(i), getName(i), getPrice(i)));
				}
				btnNextPageEnable = !isDisabled(btnNextPage);
				if (isDisplayed(btnNextPage) && btnNextPageEnable) {
					btnNextPage.click();
				}
			} while (btnNextPageEnable);

			return products;
		}
		return null;
	}

	// Get number of search item
	private int getNumberOfSearchItem() {
		waitElementDisplayed(driver, firstItem, AppConfig.MEDIUM_TIME);
		try {
			List<WebElement> elements = driver.findElements(By.xpath(parentItem));
			return elements.size();
		} catch (Exception e) {
			return 0;
		}
	}
	
	// Get price for each item.
	private Double getPrice(int index) {
		try {
			String xpath = String.format(priceXpath, index);
			WebElement element = driver.findElement(By.xpath(xpath));
			String value = getTextValue(element); // Can not get text by element.getText() method
			return convertPrice(value);
		} catch (Exception e) {
			return 0.0;
		}
	}

	// Get name for each item.
	private String getName(int index) {
		try {
			String xpath = String.format(nameXpath, index);
			WebElement element = driver.findElement(By.xpath(xpath));
			String value = getTextValue(element);
			return value.trim();
		} catch (Exception e) {
			return "";
		}
	}

	// Get link for each item.
	private String getLink(int index) {
		try {
			String xpath = String.format(linkXpath, index);
			WebElement element = driver.findElement(By.xpath(xpath));
			String value = element.getAttribute("href");
			return value.trim();
		} catch (Exception e) {
			return "";
		}
	}


	//Convert price string to double type. Ex: $469.234 to $500.5 -> 649.234
	private Double convertPrice(String input) {
		String value = "";
		if (input.contains("VND")) {
			value = input.replace("VND", "");
			value = value.replace(",", "").trim();
			try {
				double price = Double.parseDouble(value) * 0.000043;
				double roundOff = Math.round(price * 100.0) / 100.0;
				return roundOff;
			} catch (Exception e) {
				return 0.0;
			}
		}
		value = input.split(" ")[0].replace("$", "");// Some price have [from price] to [to price]
		try {
			return Double.parseDouble(value);
		} catch (Exception e) {
			return 0.0;
		}
	}

	// ---------VERIFY-----------

	/**
	 * Verify searched result is shown.
	 */
	public void verifyResultIsShown() {
		numberOfSearchResult = getNumberOfSearchItem();
		if (numberOfSearchResult == 0) {
			// Case: No item search exist
			Report.verify("No result label is shown");
			Assertion.AssertTrue(isDisplayed(lblNoResult), "No result label is not displayed correctly");
		} else {
			Report.verify("Result is shown");
		}
	}

}
