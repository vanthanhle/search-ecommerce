package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Assertion;
import common.Report;
import common.Utils;
import common.model.Product;
import testcore.AppConfig;

public class AmazonPage extends PageObjectBase {

	final private String AMAZON = "Amazone";

	/**
	 * -------------- ELEMENTS ---------------------------
	 */
	WebDriver driver;
	CommonPage common;

	@FindBy(id = "twotabsearchtextbox")
	WebElement txtSearchAmazon;

	@FindBy(id = "nav-search-submit-text")
	WebElement btnSearch;

	@FindBy(xpath = "//li[@class='a-last']//a")
	WebElement btnNextPage;

	@FindBy(xpath = "(//*[@class='a-section a-spacing-medium'])[1]")
	WebElement firstAmazonRow;

	@FindBy(xpath = "//span[.='No results for ']")
	WebElement lblNoResult;

	private String amazonSearchItemParentXpath = "//*[@class='a-section a-spacing-medium']";
	private String priceXpath = "(//*[@class='a-section a-spacing-medium'])[%s]//span[@class='a-offscreen']";
	private String nameXpath = "(//*[@class='a-section a-spacing-medium'])[%s]//a[@class='a-link-normal a-text-normal']";
	private int numberOfSearchResult;

	public AmazonPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		common = new CommonPage(driver);
	}

	/**
	 * -------------- ACTIONS ---------------------------
	 */

	/**
	 * @param value: use to enter to user name field .
	 */
	public void enterSearchValue(String value) {
		enterToElement(txtSearchAmazon, value);
		Utils.sleep(1);
	}

	/**
	 * click on search button or enter after input search field.
	 */
	public void clickOnSearchButton() {
		txtSearchAmazon.sendKeys(Keys.ENTER);
		// btnSearch.click();
		Utils.sleep(AppConfig.SMALL_TIME);
	}

	/**
	 * get result list.
	 * 
	 * @return result list.
	 */
	public List<Product> getSearchProduct() {
		if (numberOfSearchResult > 0) {
			List<Product> products = new ArrayList<Product>();
			boolean btnNextPageEnabled = false;
			do {
				numberOfSearchResult = getNumberOfSearchItem();
				for (int i = 1; i <= numberOfSearchResult; i++) {
					products.add(new Product(AMAZON, getLink(i), getName(i), getPrice(i)));
				}
				btnNextPageEnabled = isDisplayed(btnNextPage);
				if (btnNextPageEnabled) {
					btnNextPage.click();
				}
			} while (btnNextPageEnabled);

			return products;
		}
		return null;
	}

	// Get number of search item
	private int getNumberOfSearchItem() {
		waitElementDisplayed(driver, firstAmazonRow, AppConfig.MEDIUM_TIME);
		try {
			List<WebElement> elements = driver.findElements(By.xpath(amazonSearchItemParentXpath));
			return elements.size();
		} catch (Exception e) {
			return 0;
		}

	}
	
	// Get price for each item.
	private Double getPrice(int index) {
		try {
			String xpath = String.format(priceXpath, index);
			WebElement element = driver.findElement(By.xpath(xpath));
			String value = getTextValue(element); // Can not get text by element.getText() method
			return convertPrice(value);
		} catch (Exception e) {
			return 0.0;
		}
	}

	// Get name for each item.
	private String getName(int index) {
		try {
			String xpath = String.format(nameXpath, index);
			WebElement element = driver.findElement(By.xpath(xpath));
			String value = getTextValue(element);
			return value.trim();
		} catch (Exception e) {
			return "";
		}
	}

	// Get link for each item.
	private String getLink(int index) {
		try {
			String xpath = String.format(nameXpath, index);
			WebElement element = driver.findElement(By.xpath(xpath));
			String value = element.getAttribute("href");
			return value.trim();
		} catch (Exception e) {
			return "";
		}
	}


	 //Convert price string to double type. Ex: $469.234 -> 649.234
	private Double convertPrice(String input) {
		String value = input.replace("$", "");
		try {
			return Double.parseDouble(value);
		} catch (Exception e) {
			return 0.0;
		}
	}

	// ---------VERIFY-----------

	/**
	 * Verify searched result is shown.
	 */
	public void verifyResultIsShown() {
		numberOfSearchResult = getNumberOfSearchItem();
		if (numberOfSearchResult == 0) {
			// Case: No item search exist
			Report.verify("No result label is shown");
			Assertion.AssertTrue(isDisplayed(lblNoResult), "No result label is not displayed");
		} else {
			Report.verify("Result is shown");
		}
	}

}
