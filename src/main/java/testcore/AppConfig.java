package testcore;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import common.Browser;
import common.Const.Configurations;

public class AppConfig {
	/**
	 * Default constructor.
	 */
	public AppConfig() {
		// Get platform and browser variables from Jenkins
		appConfigFile = System.getenv("APP_CONFIG_FILE") != null
				? "AppConfig-" + System.getenv("APP_CONFIG_FILE") + ".xml"
				: "AppConfig.xml";

		xmlReader = new XmlReader(Configurations.MAIN_PATH + appConfigFile);
	}

	public String appConfigFile;
	private XmlReader xmlReader;
	private static AppConfig appConfigHelper;

	// Time configurations:
	public static int SMALL_TIME = Integer.parseInt(getInstant().getSmallTime());//1 second
	public static int MEDIUM_TIME = Integer.parseInt(getInstant().getMediumTime());//10 seconds
	public static int LARGE_TIME = Integer.parseInt(getInstant().getLargeTime());//60 seconds
	// Webdriver path: Window
	public static String CHROME_PATH = getInstant().getWebDriverPathWindows() + "chromedriver.exe";
	public static String FIREFOX_PATH = getInstant().getWebDriverPathWindows() + "geckodriver.exe";
	// Webdriver path: MAC
	public static String SAFARI_PATH = getInstant().getWebDriverPathMac() + "safari";
	public static String CHROME_PATH_MAC = getInstant().getWebDriverPathMac() + "chromedriver";
	public static String FIREFOX_PATH_MAC = getInstant().getWebDriverPathMac() + "geckodriver";

	// Headless mode = true: Run with no browser displayed
	public static boolean HEADLESS = getInstant().getIsHeadlessMode();
	public static List<Browser> browser = getInstant().getBrowsers();

	
	/**
	 * Get avaiable browsers from Config file.
	 */
	public List<Browser> getBrowsers() {

		ArrayList<Browser> arrBrowsers = new ArrayList<Browser>();

		// Get platform and browser variables from Jenkins
		if (System.getenv("PLATFORM") != null) {
			String osPlatform = System.getenv("PLATFORM").toUpperCase();
			String browserType = System.getenv("BrowserType");
			String[] arrBrowserName = browserType.split(",");
			for (String name : arrBrowserName) {
				arrBrowsers.add(new Browser( osPlatform, name, "unknown" ));
			}
			return arrBrowsers;
		}

		try {

			// Get browser list
			NodeList browsers = xmlReader.getNodeList("//section[@name='Browsers']/attribute[@key='Browser']");
			for (int i = 0; i < browsers.getLength(); i++) {

				boolean isEnable = XmlReader.getAttrBool((Element) browsers.item(i), "isEnable");
				if (!isEnable) {
					continue;
				}

				String platform = XmlReader.getAttr((Element) browsers.item(i), "platform");

				String version = XmlReader.getAttr((Element) browsers.item(i), "version");

				String name = XmlReader.getAttr((Element) browsers.item(i), "value");

				arrBrowsers.add(new Browser(platform, name, version));
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.toString());
			System.out.println("Could not load data from " + appConfigFile);
		}

		return arrBrowsers;
	}

	/**
	 * Get Url for run from from Config file.
	 */
	public String getUrl() {

		try {
			return xmlReader.getAttribute("//section[@name='Url']/attribute[@key='HomePage']", "value");

		} catch (Exception e) {
			System.out.println("Could not load data from " + appConfigFile);
		}
		return null;
	}

	/**
	 * Get version of Enviroment.
	 */
	public String getEnviromentVersion() {

		try {
			return xmlReader.getAttribute("//section[@name='Url']/attribute[@key='HomePage']", "version");

		} catch (Exception e) {
			System.out.println("Could not load data from " + appConfigFile);
		}

		return null;
	}

	/**
	 * Get Url for run from from Config file.
	 */
	public String getSmallTime() {
		return getConfigValue("Time", "SMALL_TIME");
	}

	/**
	 * Get Url for run from from Config file.
	 */
	public String getMediumTime() {
		return getConfigValue("Time", "MEDIUM_TIME");
	}

	/**
	 * Get Url for run from from Config file.
	 */
	public String getLargeTime() {
		return getConfigValue("Time", "LARGE_TIME");
	}

	/**
	 * Headless mode.
	 */
	public boolean getIsHeadlessMode() {
		return getConfigValue("Helper", "HeadlessMode").equalsIgnoreCase("true");
	}

	/**
	 * Get quit driver state.
	 */
	public boolean getIsQuitDriver() {
		return getConfigValue("Helper", "QuitDriver").equalsIgnoreCase("true");
	}

	/**
	 * Get web driver path of Windows.
	 */
	public String getWebDriverPathWindows() {
		return getConfigValue("WebDriverPath", "Windows");
	}

	/**
	 * Get web driver path of Mac.
	 */
	public String getWebDriverPathMac() {
		return getConfigValue("WebDriverPath", "Mac");
	}

	// Get configuration by key, section.
	private String getConfigValue(String section, String key) {

		try {
			String path = String.format("//section[@name='%s']/attribute[@key='%s']", section, key);
			return xmlReader.getAttribute(path, "value");

		} catch (Exception e) {
			System.out.println("Could not load data from " + appConfigFile);
		}
		return null;
	}

	/**
	 * @return new instant of Appconfig file.
	 */
	public static AppConfig getInstant() {
		if (appConfigHelper == null) {
			appConfigHelper = new AppConfig();
		}
		return appConfigHelper;
	}
}
