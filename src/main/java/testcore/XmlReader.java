package testcore;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlReader {
	private DocumentBuilderFactory dbFactory;
	private DocumentBuilder documentBuilder;
	private Document doc;
	private XPathFactory xpathfactory;
	private XPath xpath;
	private String filePath;

	public XmlReader(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * Load Xml document.
	 * 
	 * @example https://howtodoinjava.com/xml/java-xpath-tutorial-example/
	 */
	private void loadDocument() throws ParserConfigurationException, SAXException, IOException {

		if (doc == null) {

			File xmlFile = new File(this.filePath);
			dbFactory = DocumentBuilderFactory.newInstance();
			documentBuilder = dbFactory.newDocumentBuilder();
			doc = documentBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			xpathfactory = XPathFactory.newInstance();
			xpath = xpathfactory.newXPath();
		}
	}

	/**
	 * Get node list by xPath.
	 * 
	 * @return NodeList
	 */
	public NodeList getNodeList(String xmlPath)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		loadDocument();
		XPathExpression expr = xpath.compile(xmlPath);
		Object result = expr.evaluate(doc, XPathConstants.NODESET);
		return (NodeList) result;
	}

	/**
	 * Get element by xPath.
	 * 
	 * @return Element
	 */
	public Element getElement(String xpath, int index)
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {

		NodeList nodeList = getNodeList(xpath);
		return (Element) nodeList.item(index);
	}

	/**
	 * Get first element by xPath.
	 * 
	 * @return Element
	 */
	public Element getFirstElement(String xpath)
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {

		return getElement(xpath, 0);
	}

	/**
	 * Get attribute of first element by xPath.
	 * 
	 * @return String
	 */
	public String getAttribute(String xpath, String attr)
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {

		return getFirstElement(xpath).getAttribute(attr);
	}

	public boolean getAttributeBool(String xpath, String attr)
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		String value = getAttribute(xpath, attr);
		return value.toUpperCase().contains("TRUE");
	}

	/**
	 * Get attribute by xPath in Integer.
	 * 
	 * @param xpath xPath
	 * @param attr  attribute
	 * @return Integer
	 */
	public int getAttributeInt(String xpath, String attr)
			throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		String value = getAttribute(xpath, attr);
		return Integer.parseInt(value);
	}

	/**
	 * Get Attribute of an element in String.
	 * 
	 * @return String
	 */
	public static String getAttr(Element element, String attr) {
		return element.getAttribute(attr);
	}

	/**
	 * Get Attribute of an element in Boolean.
	 * 
	 * @return Boolean
	 */
	public static boolean getAttrBool(Element element, String attr) {
		return element.getAttribute(attr).toUpperCase().contains("TRUE");
	}

	/**
	 * Get Attribute of an element in Integer.
	 * 
	 * @return Integer
	 */
	public static int getAttrInt(Element element, String attr) {
		return Integer.parseInt(element.getAttribute(attr));
	}
}
