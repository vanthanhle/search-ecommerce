package testcore;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import common.Report;
import common.Utils;

public class WebDriverFactory {

	/**
	 * @return web driver.
	 */
	public WebDriver initDriver(String browser) {
		String currentBroser = browser;
		if (Utils.isNullOrEmpty(currentBroser)) {
			currentBroser = AppConfig.getInstant().getBrowsers().get(0).getName();
		}
		switch (currentBroser.toLowerCase()) {
		case "chrome":
			return this.initChrome();
		case "firefox":
			return this.initFireFox();
		default:
			return this.initChrome();
		}
	}

	private WebDriver initChrome() {
		WebDriver driver;
		if (Utils.isMacDevice()) {
			System.setProperty("webdriver.chrome.driver", AppConfig.CHROME_PATH_MAC);
		} else {
			System.setProperty("webdriver.chrome.driver", AppConfig.CHROME_PATH);
		}

		ChromeOptions options = new ChromeOptions();
		if (AppConfig.HEADLESS) {
			options.addArguments("headless");
			options.addArguments("window-size=1200x600");
		}
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		this.logBrowser(driver);
		return driver;
	}

	private WebDriver initFireFox() {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", AppConfig.FIREFOX_PATH);
		FirefoxOptions options = new FirefoxOptions();
		if (AppConfig.HEADLESS) {
			options.addArguments("-headless");
			options.addArguments("window-size=1200x600");
		}
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		this.logBrowser(driver);
		return driver;
	}
	
	private void logBrowser(WebDriver driver) {
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();
		Report.step(String.format("Initialize [%s] browser, version: %s %s", browserName, browserVersion, AppConfig.HEADLESS?". Headless mode":""));
	}

}
