package testscripts.search;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import common.Const.Configurations;
import common.ExcelHelper;
import common.Report;
import common.model.Excel;
import common.model.Product;
import pages.AmazonPage;
import pages.CommonPage;
import pages.EbayPage;
import testscripts.TestScriptBase;

public class VerifySearchFunction extends TestScriptBase {

	List<Product> products = new ArrayList<Product>();
	Excel excel = new Excel("data/data.xlsx", "search");
	String[][] data = null;
	String amazonUrl;
	String ebayUrl;
	String amazonTitle;
	String ebayTitle;
	String searchInput;

	@Override
	public void setUp() {
		commonPage = new CommonPage(driver);
		amazonPage = new AmazonPage(driver);
		ebayPage = new EbayPage(driver);
		// Get all needed data from excel file:
		data = ExcelHelper.getAllData(excel);
		amazonUrl = ExcelHelper.getData("Path1", Configurations.INPUT_KEY);
		ebayUrl = ExcelHelper.getData("Path2", Configurations.INPUT_KEY);
		amazonTitle = ExcelHelper.getData("AMAZON_TITLE", Configurations.EXPECTED_KEY);
		ebayTitle = ExcelHelper.getData("EBAY_TITLE", Configurations.EXPECTED_KEY);
		searchInput = ExcelHelper.getData("SEARCH_INPUT", Configurations.INPUT_KEY);
	}

	/**
	 * Verify search function.
	 */
	@Test(priority = 0)
	public void verifySearchFunction() {

		Report.step("Step 1: Go to Amazone page");
		commonPage.goToPage(amazonUrl);

		Report.verify("Verify Amazon page is displayed");
		commonPage.verifyTitle(amazonTitle);

		Report.step("Step 2: Enter Amazone search value: " + searchInput);
		amazonPage.enterSearchValue(searchInput);

		Report.step("Step 3: Click on search button");
		amazonPage.clickOnSearchButton();
		amazonPage.verifyResultIsShown();
		products = amazonPage.getSearchProduct();

		Report.step("Step 4: Go to Ebay page");
		commonPage.goToPage(ebayUrl);

		Report.verify("Verify Ebay page is displayed");
		commonPage.verifyTitle(ebayTitle);

		Report.step("Step 5: Enter Ebay search value: " + searchInput);
		ebayPage.enterSearchValue(searchInput);

		Report.step("Step 6: Click on search button");
		ebayPage.clickOnSearchButton();
		ebayPage.verifyResultIsShown();

		Report.step("Step 7: Combine the outputs of both website");
		if (products == null) {
			products = ebayPage.getSearchProduct();
		} else {
			List<Product> ebays = ebayPage.getSearchProduct();
			if (ebays != null) {
				products.addAll(ebays);
			}
		}
		
		Report.step("Step 8: Display the result in ascending order of price.");
		commonPage.displayedTheResult(products);
	}

	@Override
	public void cleanUp() {
		// Clean data
	}

}
