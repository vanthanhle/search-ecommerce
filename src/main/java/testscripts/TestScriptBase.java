package testscripts;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import common.Logger;
import pages.AmazonPage;
import pages.CommonPage;
import pages.EbayPage;
import testcore.WebDriverFactory;

public abstract class TestScriptBase {

	public WebDriver driver;
	public WebDriverFactory webDriverFactory = new WebDriverFactory();
	protected CommonPage commonPage;
	protected AmazonPage amazonPage;
	protected EbayPage ebayPage;

	@BeforeSuite
	public void beforeSuite() {

	}

	@BeforeClass
	@Parameters({"browser"})
	public void initialize(@Optional("") String browser) {
		driver = webDriverFactory.initDriver(browser);
		this.setUp();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		Logger.start(method.getName());
	}

	@AfterMethod
	public void afterMethod() {
		Logger.drawLine();
	}

	@AfterTest
	public void dispose() {
		this.cleanUp();
		driver.quit();
	}

	@AfterSuite
	public void afterSuite() {
		// driver.quit();
	}

	public abstract void setUp();

	public abstract void cleanUp();

	
}
