package common;

public class Browser {
	private String platform;
	private String name;
	private String version;
	
	public Browser(String platform, String name, String version) {
		this.platform = platform;
		this.name = name;
		this.version = version;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
