package common;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class Utils {
	
	//--------General functions------------------
	/**
	 * Sleep.
	 * @param second
	 */
	public static void sleep(int second) {
		try {
			int mlSecond = second * 1000;
			Thread.sleep(mlSecond);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
     * Check running code is mac device.
     */
    public static boolean isMacDevice() {
        String os = System.getProperty("os.name").toLowerCase();
        return (os.contains("mac"));
    }

	
	/**
     * Check string is null or empty.
     * 
     */
    public static boolean isNullOrEmpty(String str) {

        return (str == null || str.isEmpty());
    }


	/**
	 * Copy array from another array.
	 * @param source
	 * @param start
	 * @param end
	 * @return
	 */
	public static String[] copyArray(String[] source, int start, int end) {
		return Arrays.copyOfRange(source, start, end);
	}
	
	/**
	 * Export data to text file.
	 * @param path
	 * @param value
	 */
	public static void ExportDataToTextFile(String path, String value) {
	  BufferedWriter writer = null;
	    try {
	        writer = new BufferedWriter(new FileWriter(path));
	        writer.write(value);
	    } catch (IOException e) {
	        System.err.println(e);
	    } finally {
	        if (writer != null) {
	            try {
	                writer.close();
	            } catch (IOException e) {
	                System.err.println(e);
	            }
	        }
	    }
	}
}
