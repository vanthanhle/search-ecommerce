package common;

public class Const {
	
	public static class Configurations{
		
		public static String MAIN_PATH = "src/main/resources/";
		public static String EXPORT_PATH = "./output/output_%s.txt";
		public static String INPUT_KEY = "Input";
		public static String EXPECTED_KEY = "Expectations";
		
	}

}
