package common.model;

public class Product implements Comparable<Product>{
  public Product(String website, String link, String name, double price) {
    super();
    this.website = website;
    this.link = link;
    this.name = name;
    this.price = price;
  }
  private String website;
  private String link;
  private String name;
  private double price;

  
  public String getWebsite() {
    return website;
  }
  public void setWebsite(String website) {
    this.website = website;
  }
  public String getLink() {
    return link;
  }
  public void setLink(String link) {
    this.link = link;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(int price) {
    this.price = price;
  }
  public double getPrice() {
    return price;
  }
  public void setPrice(double price) {
    this.price = price;
  }
  


  
  @Override
  public String toString() {
    return String.format(" %-7s| %-30.30s| %-10.10s| %s", website, name, price, link);
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(price);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Product other = (Product) obj;
    if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
      return false;
    return true;
  }
  
  public int compareTo(Product o) {
    
    double compareage=((Product)o).getPrice();
    /* For Ascending order*/
    return (int) (this.price-compareage);
  }
  

  
}
