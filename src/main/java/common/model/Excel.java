package common.model;

import common.Const.Configurations;

public class Excel {

	private String pathName;
	private String sheetName;
	
	public Excel(String pathName, String sheetName) {
		super();
		this.pathName = Configurations.MAIN_PATH + pathName;
		this.sheetName = sheetName;
	}
	
	public String getPathName() {
		return pathName;
	}
	public void setPathName(String pathName) {
		this.pathName = pathName;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	
}
