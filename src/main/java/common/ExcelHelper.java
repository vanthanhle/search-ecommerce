package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import common.model.Excel;



public class ExcelHelper {
	
	public static ExcelHelper excel;
	private static String data[][];
	private static String[] colKeys;
	private static String[] rowKeys;
	
	public String[] getColKeys() {
		return colKeys;
	}

	public String[] getRowKeys() {
		return rowKeys;
	}

	private static int COL_KEY_INDEX = 0;
	private static int ROW_KEY_INDEX = 1;
	
	public static String[][] getDataFromExcel(){
		return data;
	}
	
	public static boolean hasData(){
		if (data == null) {
			return false;
		}
		return true;
	}
	
	public static ExcelHelper getInstance(){
		if (excel == null) {
			excel = new ExcelHelper();
		}
		return excel;
	}
	
	public static String[][] getInputData(Excel excel, String fromKey, String toKey){
		String[][] data = getAllData(excel);
		int start = getIndexOfColKey(fromKey);
		int end = getIndexOfColKey(toKey);
		int START_DATA_INDEX = 2;
		int length = (end - start) + 1;
		String[][] output = new String[data.length - START_DATA_INDEX][length];//3 first rows not necessary 
		int j = 0;
		for (int i = START_DATA_INDEX; i < data.length; i++) {
			output[j] = Utils.copyArray(data[i], start-1, end);
			j ++;
		}
		return output;
	}
	
	public static String[][] getOneRowSample(String[][] source,int row){
		int length = source[0].length;
		String[][] output = new String[1][length];
		output[0] = Utils.copyArray(source[row], 0, length);
		return output;
	}
	
	/**
	 * click to full screen button
	 */
	public static String[][] getAllData(Excel excel){
		loadData(excel);
		String[][] values = getDataFromExcel();
		return values;
	}
	
	
	public static void loadData(Excel excel){
		loadExcelData(excel.getPathName(), excel.getSheetName());
	}
	 
	 public static String getData(int rowIndex, int colIndex){
		 String value = data[rowIndex][colIndex];
		 return value;
	 }
	 
	 public static String getData(String rowKey, String colKey){
		 int rowIndex = getIndexOfRowKey(rowKey)+1;
		  int colIndex = getIndexOfColKey(colKey) ;
		  return getData(rowIndex, colIndex);
	 }
	 
	 
	 
	/**
	   * Get List data in export file by [key] value.
	   * 
	   */
	  public static String[] getCollumnByKey(String colKey) {
	    return getCollumn(getIndexOfColKey(colKey));
	  }
	  
	  /**
	   * Get List data in export file by index.
	   * 
	   */
	  private static String[] getCollumn(int index) {
		  String[] output = new String[data.length - 1];
		  int j = 0;
		  for (int i = 1; i < data.length; i++) {
			  output[j] = data[i][index];
			  j++;
		  }
		  return output;
	  }

	/**
	 * @param rowKey
	 * @return
	 */
	private static int getIndexOfRowKey(String rowKey) {
		int index = -1;
		for (int i = 0; i < colKeys.length; i++) {
			index = Arrays.asList(getCollumn(i)).indexOf(rowKey);
			if (index > 0) {
				return index;
			}
		}
		return index;
	}
	  
	  /**
	 * @param colKey
	 * @return
	 */
	public static int getIndexOfColKey(String colKey){
		  int index = Arrays.asList(colKeys).indexOf(colKey);
		  return index;
	  }
	  
	/**
	   * Get List of data from excel and save to array.
	   * 
	   */
	public static void loadExcelData(String fullPath, String sheetName) {
		XSSFSheet sheet = getSheetContentXlsx(fullPath, sheetName);
		if (sheet == null) {
			data = null;
		}
		int length = sheet.getRow(ROW_KEY_INDEX).getLastCellNum();
		data = new String[sheet.getLastRowNum() + 1][length];
		int i = 0;
		for (Row row : sheet) {
			for (int j = 0; j < length; j++) {
				Cell cel1 = row.getCell(j);//cell.getStringCellValue().trim();
				try {
					data[i][j] = cel1.getStringCellValue().trim();
				} catch (IllegalStateException e) {
					data[i][j] = String.valueOf((int)cel1.getNumericCellValue());
				} catch (Exception e) {
					data[i][j] = "";
				}
			}
			i++;
		}
	    
	   colKeys = data[ROW_KEY_INDEX];
	   rowKeys = getCollumn(COL_KEY_INDEX);
	  }
	  
	  /**
	   * Get sheet content from xlsx file.
	   * 
	   */
	  @SuppressWarnings("resource")
	private static XSSFSheet getSheetContentXlsx(String xlsFullPath, String sheetName) {
	    FileInputStream inputStream = null;
	    XSSFWorkbook workbook = null;
	    try {
	      inputStream = new FileInputStream(new File(xlsFullPath));
	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    }
	    try {
	      workbook = new XSSFWorkbook(inputStream);
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    return workbook.getSheet(sheetName);
	  }

}
