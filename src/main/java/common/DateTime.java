package common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTime {
	public static String getTimeNow(String formatPattern) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatPattern);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}

}
